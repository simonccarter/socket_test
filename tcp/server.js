var net = require('net');
var server = net.createServer(function(socket) {

	server.getConnections(function counter(err,count){
		console.log("there are " + count + " connections");	
	});

	// Handle incoming messages from clients.
	socket.on('data', function (data) {
		broadcast(socket.name + "> " + data, socket);
	});

	// Remove the client from the list when it leaves
	socket.on('end', function () {
		
	});

});
 
server.listen(15432);
