module.exports = function(server, express) {
	io = require('socket.io').listen(server);
	var activeClients = 0;

	//TODO: CHANGE sockets hash to redis store...
	io.sockets.on('connection', function (socket) {
		activeClients+=1;
		if(activeClients%100==0)
			console.log("new connection. active sockets: " + activeClients);
			socket.on('private message', function(data){
	//		console.log("data: " + data);
		});
		socket.on('disconnect', function () {
			console.log("Here in socket disconnect for:");
			console.log("disconnection. active sockets: " + activeClients);
			activeClients-=1
		});
        });
	return io; 
}


