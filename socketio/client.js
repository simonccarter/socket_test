var io = require('socket.io-client');
var sockets = [];
for (var i = 0; i < 1000; i++){
	var socket = io.connect('http://81.4.107.88:15432', {'force new connection': true});
	socket.on('connect', function () {
//		console.log("socket connected"); 
		socket.emit('private message', { user: 'me', msg: 'whazzzup?' });
	});
	socket.on('disconnect', function () {
		console.log("socket disconnected"); 
		socket.emit('private message', { user: 'me', msg: 'whazzzup?' });
	});
	sockets.push(socket);
}
