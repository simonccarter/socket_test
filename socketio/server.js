var express = require('express');

//SET UP SERVER VARIABLES
var fullPath = __dirname;
var DEBUG = false;
var port = 15432;

var app = module.exports = express();

app.configure(function(){
  app.use(express.compress());
  app.use(express.logger());
  app.use(express.bodyParser());
  app.use(express.methodOverride());

  app.use(express.static(__dirname + '/public'));
  app.use(express.static(__dirname + '/assets'));
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

//START SERVER
server = require('http').createServer(app)
server.listen(port);
io = require('./sockets')(server, express);
console.log('Socket listening on port ' + port);

